﻿using System.Diagnostics;
using Moq;
using NUnit.Framework;
using TicTacToe.Game;

namespace TicTacToe.Tests.Game
{
    [TestFixture]
    public sealed class ViewModelTests
    {
        [Test]
        public void Board_Test()
        {
            var model = new Model(2, 2);
            var viewModel = new ViewModel(model);

            Assert.AreEqual(model.Board, viewModel.Board);
        }

        [Test]
        public void Step_Test()
        {
            var model = new Mock<IModel>();
            // ReSharper disable once PossibleNullReferenceException
            model.Setup(m => m.ComputerStep()).Verifiable();

            Debug.Assert(model.Object != null, "model.Object != null");
            var viewModel = new ViewModel(model.Object);

            viewModel.Step();

            model.Verify(m => m.ComputerStep(), Times.Once);
        }
    }
}