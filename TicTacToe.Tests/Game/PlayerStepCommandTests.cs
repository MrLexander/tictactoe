﻿using System.Diagnostics;
using Moq;
using NUnit.Framework;
using TicTacToe.Game;

namespace TicTacToe.Tests.Game
{
    [TestFixture]
    public sealed class PlayerStepCommandTests
    {
        [Test]
        public void CanExecute_Empty_True()
        {
            var model = new Model(2, 2);
            var computerStepProvider = new Mock<IComputerStepProvider>();

            Debug.Assert(computerStepProvider.Object != null, "computerStepProvider.Object != null");
            var playerStepCommand = new PlayerStepCommand(model, computerStepProvider.Object);

            var canExecute = playerStepCommand.CanExecute(new Point(0, 0));

            Assert.IsTrue(canExecute);
        }

        [Test]
        public void CanExecute_X_False()
        {
            var coordinates = new Point(0, 0);

            var model = new Model(2, 2);
            model.PlayerOn(coordinates, Player.X);

            var computerStepProvider = new Mock<IComputerStepProvider>();

            Debug.Assert(computerStepProvider.Object != null, "computerStepProvider.Object != null");
            var playerStepCommand = new PlayerStepCommand(model, computerStepProvider.Object);

            var canExecute = playerStepCommand.CanExecute(coordinates);

            Assert.IsFalse(canExecute);
        }

        [Test]
        public void Execute_Test()
        {
            var model = new Model(2, 2);
            var computerStepProvider = new Mock<IComputerStepProvider>();
            // ReSharper disable once PossibleNullReferenceException
            computerStepProvider.Setup(c => c.Step()).Callback(model.ComputerStep);

            Debug.Assert(computerStepProvider.Object != null, "computerStepProvider.Object != null");
            var playerStepCommand = new PlayerStepCommand(model, computerStepProvider.Object);

            var xStepCoordinate = new Point(0, 0);
            playerStepCommand.Execute(xStepCoordinate);

            const int emptyCount = 2;
            Assert.AreEqual(emptyCount, model.Board.EmptyCells.Count);
        }
    }
}