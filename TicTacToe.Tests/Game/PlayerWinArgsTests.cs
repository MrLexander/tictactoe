﻿using NUnit.Framework;
using TicTacToe.Game;

namespace TicTacToe.Tests.Game
{
    [TestFixture]
    public sealed class PlayerWinArgsTests
    {
        [Test]
        public void Ctor_Test()
        {
            const Solution solution = Solution.Draw;
            var playerWinArgs = new PlayerWinArgs(solution);
            Assert.AreEqual(solution, playerWinArgs.Winner);
        }
    }
}