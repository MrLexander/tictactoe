﻿using NUnit.Framework;
using TicTacToe.Game;

namespace TicTacToe.Tests.Game
{
    [TestFixture]
    public sealed class ModelTests
    {
        [Test]
        public void ComputerStep_Test()
        {
            var model = new Model(2, 2);
            model.PlayerOn(new Point(0, 0), Player.X);

            var oldBoard = model.Board.GetCopy();

            model.ComputerStep();
            var newBoard = model.Board.GetCopy();

            CollectionAssert.AreNotEquivalent(oldBoard.BoardField, newBoard.BoardField);
        }

        [Test]
        public void PlayerOnZeroZero_Test()
        {
            const Player expected = Player.X;
            var coordinates = new Point(0, 0);

            var model = new Model(2, 2);
            model.PlayerOn(coordinates, expected);

            Assert.AreEqual(expected, model.Board[coordinates.Y][coordinates.X]);
        }
    }
}