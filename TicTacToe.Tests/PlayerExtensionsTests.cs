﻿using NUnit.Framework;

namespace TicTacToe.Tests
{
    [TestFixture]
    public sealed class PlayerExtensionsTests
    {
        [Test]
        public void PlayerEmpty_ReturnsStringWithSpace()
        {
            var cellText = Player.Empty.GetCellText();

            const string expectedText = " ";
            Assert.AreEqual(expectedText, cellText);
        }

        [Test]
        public void PlayerO_ReturnsO()
        {
            var cellText = Player.O.GetCellText();

            const string expectedText = "O";
            Assert.AreEqual(expectedText, cellText);
        }

        [Test]
        public void PlayerX_ReturnsX()
        {
            var cellText = Player.X.GetCellText();

            const string expectedText = "X";
            Assert.AreEqual(expectedText, cellText);
        }
    }
}