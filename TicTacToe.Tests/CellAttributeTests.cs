﻿using NUnit.Framework;

namespace TicTacToe.Tests
{
    [TestFixture]
    public sealed class CellAttributeTests
    {
        [Test]
        public void Ctor_Test()
        {
            const string text = "text";
            var cellAttribute = new CellAttribute(text);

            Assert.AreEqual(text, cellAttribute.Text);
        }
    }
}