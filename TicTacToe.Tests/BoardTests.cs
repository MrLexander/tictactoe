﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace TicTacToe.Tests
{
    [TestFixture]
    public sealed class BoardTests
    {
        [Test]
        public void Ctor_Test()
        {
            var board = new Board(2);

            foreach (var player in board.BoardField.SelectMany(row => row))
            {
                const Player expected = Player.Empty;
                Assert.AreEqual(expected, player);
            }
        }

        [Test]
        public void EmptyCells_Test()
        {
            var board = new Board(2);
            board = board.PlayerOn(new Point(0, 0), Player.O);

            var expectedCells = new List<Point> {new Point(0, 1), new Point(1, 0), new Point(1, 1)};

            CollectionAssert.AreEquivalent(expectedCells, board.EmptyCells);
        }

        [Test]
        public void GetCopy_Test()
        {
            var board = new Board(2);
            var newBoard = board.GetCopy();

            CollectionAssert.AreEqual(board.BoardField, newBoard.BoardField);
        }

        [Test]
        public void GetWinner_AllIsEmpty_Unknown()
        {
            var board = new Board(2);
            const Solution expected = Solution.Unknown;
            Assert.AreEqual(expected, board.GetWinner());
        }

        [Test]
        public void GetWinner_Draw()
        {
            var board = new Board(3);
            board = board.PlayerOn(new Point(0, 0), Player.O);
            board = board.PlayerOn(new Point(0, 1), Player.X);
            board = board.PlayerOn(new Point(0, 2), Player.O);
            board = board.PlayerOn(new Point(1, 0), Player.X);
            board = board.PlayerOn(new Point(1, 1), Player.X);
            board = board.PlayerOn(new Point(1, 2), Player.O);
            board = board.PlayerOn(new Point(2, 0), Player.O);
            board = board.PlayerOn(new Point(2, 1), Player.O);
            board = board.PlayerOn(new Point(2, 2), Player.X);

            const Solution expected = Solution.Draw;
            Assert.AreEqual(expected, board.GetWinner());
        }

        [Test]
        public void GetWinner_OByBackDiagonal_OWin()
        {
            var board = new Board(2);
            board = board.PlayerOn(new Point(1, 0), Player.O);
            board = board.PlayerOn(new Point(0, 1), Player.O);

            const Solution expected = Solution.OWin;
            Assert.AreEqual(expected, board.GetWinner());
        }

        [Test]
        public void GetWinner_OByDiagonal_OWin()
        {
            var board = new Board(2);
            board = board.PlayerOn(new Point(0, 0), Player.O);
            board = board.PlayerOn(new Point(1, 1), Player.O);

            const Solution expected = Solution.OWin;
            Assert.AreEqual(expected, board.GetWinner());
        }

        [Test]
        public void GetWinner_OByHorizontal_OWin()
        {
            var board = new Board(2);
            board = board.PlayerOn(new Point(0, 1), Player.O);
            board = board.PlayerOn(new Point(1, 1), Player.O);

            const Solution expected = Solution.OWin;
            Assert.AreEqual(expected, board.GetWinner());
        }

        [Test]
        public void GetWinner_OByVertical_OWin()
        {
            var board = new Board(2);
            board = board.PlayerOn(new Point(1, 0), Player.O);
            board = board.PlayerOn(new Point(1, 1), Player.O);

            const Solution expected = Solution.OWin;
            Assert.AreEqual(expected, board.GetWinner());
        }

        [Test]
        public void GetWinner_XByBackDiagonal_XWin()
        {
            var board = new Board(2);
            board = board.PlayerOn(new Point(1, 0), Player.X);
            board = board.PlayerOn(new Point(0, 1), Player.X);

            const Solution expected = Solution.XWin;
            Assert.AreEqual(expected, board.GetWinner());
        }

        [Test]
        public void GetWinner_XByDiagonal_XWin()
        {
            var board = new Board(2);
            board = board.PlayerOn(new Point(0, 0), Player.X);
            board = board.PlayerOn(new Point(1, 1), Player.X);

            const Solution expected = Solution.XWin;
            Assert.AreEqual(expected, board.GetWinner());
        }

        [Test]
        public void GetWinner_XByHorizontal_XWin()
        {
            var board = new Board(2);
            board = board.PlayerOn(new Point(0, 1), Player.X);
            board = board.PlayerOn(new Point(1, 1), Player.X);

            const Solution expected = Solution.XWin;
            Assert.AreEqual(expected, board.GetWinner());
        }

        [Test]
        public void GetWinner_XByVertical_XWin()
        {
            var board = new Board(2);
            board = board.PlayerOn(new Point(1, 0), Player.X);
            board = board.PlayerOn(new Point(1, 1), Player.X);

            const Solution expected = Solution.XWin;
            Assert.AreEqual(expected, board.GetWinner());
        }

        [Test]
        public void SetPlayerOn_Test()
        {
            const Player player = Player.O;

            var board = new Board(2);
            var coordinates = new Point(0, 0);
            var newBoard = board.PlayerOn(coordinates, player);

            CollectionAssert.AreNotEqual(board.BoardField, newBoard.BoardField);
            Assert.AreEqual(Player.Empty, board[coordinates.Y][coordinates.X]);
            Assert.AreEqual(player, newBoard[coordinates.Y][coordinates.X]);
        }
    }
}