﻿using NUnit.Framework;

namespace TicTacToe.Tests
{
    [TestFixture]
    public sealed class PointTests
    {
        [Test]
        public void Ctor_Test()
        {
            const int x = 0;
            const int y = 0;
            var point = new Point(x, y);

            Assert.AreEqual(x, point.X);
            Assert.AreEqual(y, point.Y);
        }
    }
}