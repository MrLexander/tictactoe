﻿using System.Runtime.CompilerServices;
using JetBrains.Annotations;

namespace Guard
{
    [PublicAPI]
    public static class ThrowIf
    {
        public static class Variable
        {
            [ContractAnnotation("variable:null=>halt")]
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public static void IsNull<T>(T variable, [NotNull] string variableName)
                where T : class
            {
                if (variable == null)
                {
                    ThrowHelper.NullReference(variableName);
                }
            }
        }

        public static class Argument
        {
            [ContractAnnotation("argument:null=>halt")]
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public static void IsNull<T>(T argument, [NotNull] string argumentName)
                where T : class
            {
                if (argument == null)
                {
                    ThrowHelper.ArgumentNull(argumentName);
                }
            }
        }
    }
}