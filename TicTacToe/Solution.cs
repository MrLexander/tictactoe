﻿namespace TicTacToe
{
    internal enum Solution
    {
        XWin = 0,
        Unknown = 1,
        Draw = 2,
        OWin = 3,
        Default = int.MaxValue
    }
}