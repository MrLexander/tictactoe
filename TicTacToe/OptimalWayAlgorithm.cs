﻿using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Guard;
using JetBrains.Annotations;

namespace TicTacToe
{
    internal sealed class OptimalWayAlgorithm
    {
        private readonly int _depth;

        internal OptimalWayAlgorithm(int depth)
        {
            _depth = depth;
        }

        internal Point Get([NotNull] Board board)
        {
            ThrowIf.Argument.IsNull(board, nameof(board));
            Minimax(board, 0, out var point);
            return point;
        }

        private Solution Minimax([NotNull] Board board, int currentDepth, out Point point)
        {
            var nextDepth = currentDepth + 1;

            var emptyCells = board.EmptyCells;

            if (currentDepth > _depth)
            {
                point = emptyCells.First();
                return Solution.Unknown;
            }

            Point? wrostPoint = null;
            var wrostSolution = Solution.Default;

            Parallel.ForEach(emptyCells, emptyCell =>
            {
                var newBoard = board.GetCopy();

                var player = currentDepth % 2 != 0 ? Player.O : Player.X;

                var row = newBoard[emptyCell.Y];
                Debug.Assert(row != null, nameof(row) + " != null");
                row[emptyCell.X] = player;

                var preTestSolution = newBoard.GetWinner();
                if (preTestSolution != Solution.Unknown)
                {
                    wrostSolution = preTestSolution;
                    wrostPoint = emptyCell;
                }
                else
                {
                    var testSolution = Minimax(newBoard, nextDepth, out var testPoint);

                    if (testSolution < wrostSolution)
                    {
                        wrostPoint = testPoint;
                        wrostSolution = testSolution;
                    }
                }

                row[emptyCell.X] = Player.Empty;
            });

            Debug.Assert(wrostPoint != null, nameof(wrostPoint) + " != null");
            point = wrostPoint.Value;
            switch (wrostSolution)
            {
                case Solution.XWin:
                    return Solution.OWin;
                case Solution.OWin:
                    return Solution.XWin;
                default:
                    return wrostSolution;
            }
        }
    }
}