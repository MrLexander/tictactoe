﻿using JetBrains.Annotations;
using StartView = TicTacToe.Start.View;
using StartViewModel = TicTacToe.Start.ViewModel;
using GameView = TicTacToe.Game.View;
using GameViewModel = TicTacToe.Game.ViewModel;
using GameModel = TicTacToe.Game.Model;

namespace TicTacToe
{
    internal sealed class Starter
    {
        [NotNull] private readonly StartView _starter;
        [NotNull] private readonly StartViewModel _starterViewModel;

        internal Starter()
        {
            _starterViewModel = new StartViewModel();
            _starter = new StartView(_starterViewModel);
        }

        internal void Invoke()
        {
            while (true)
            {
                _starter.ShowDialog();
                if (!_starter.IsStart)
                {
                    return;
                }

                var level = _starterViewModel.Level;
                var capacity = _starterViewModel.Capacity;

                var gameModel = new GameModel(capacity, level);
                var gameViewModel = new GameViewModel(gameModel);
                var gameView = new GameView(gameViewModel);
                gameView.ShowDialog();
            }
        }
    }
}