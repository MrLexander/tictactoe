﻿using System.Windows;
using Guard;
using JetBrains.Annotations;

namespace TicTacToe.Start
{
    public sealed partial class View
    {
        internal View([NotNull] ViewModel viewModel)
        {
            ThrowIf.Argument.IsNull(viewModel, nameof(viewModel));
            DataContext = viewModel;

            Closed += (sender, args) => IsStart = false;

            InitializeComponent();
        }

        internal bool IsStart { get; private set; }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            IsStart = true;
            Visibility = Visibility.Hidden;
        }
    }
}