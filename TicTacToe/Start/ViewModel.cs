﻿using System.Collections.ObjectModel;
using JetBrains.Annotations;

namespace TicTacToe.Start
{
    internal sealed class ViewModel
    {
        public const double MaxLevel = 4;
        public const int Interval = 1;
        private const int MinCapacity = 2;
        private const int MaxCapacity = 4;
        private int _capacity;

        internal ViewModel()
        {
            Level = 3;
            Capacity = 3;
        }

        public static double MinLevel => 1;

        public static ObservableCollection<int> Capacities
        {
            get
            {
                var observableCollection = new ObservableCollection<int>();
                for (var i = MinCapacity; i <= MaxCapacity; i++)
                {
                    observableCollection.Add(i);
                }

                return observableCollection;
            }
        }

        [UsedImplicitly]
        public int Level { get; set; }

        [UsedImplicitly]
        public int Capacity
        {
            get => _capacity;
            set
            {
                if (value >= MinCapacity && value <= MaxCapacity)
                {
                    _capacity = value;
                }
            }
        }
    }
}