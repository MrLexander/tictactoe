﻿using System;
using Guard;
using JetBrains.Annotations;

namespace TicTacToe
{
    [AttributeUsage(AttributeTargets.Field)]
    internal sealed class CellAttribute : Attribute
    {
        internal CellAttribute([NotNull] string text)
        {
            ThrowIf.Argument.IsNull(text, nameof(text));
            Text = text;
        }

        [NotNull]
        internal string Text { get; }
    }
}