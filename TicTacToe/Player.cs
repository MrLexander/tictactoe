﻿namespace TicTacToe
{
    internal enum Player
    {
        [Cell(" ")] Empty,
        [Cell("X")] X,
        [Cell("O")] O
    }
}