﻿using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace TicTacToe
{
    internal sealed class Board
    {
        [ItemNotNull] [NotNull] private readonly List<List<Player>> _board;

        private Board([NotNull] List<List<Player>> board)
        {
            _board = board;
        }

        internal Board(int capacity)
        {
            _board = InitializeBoard(capacity);
        }

        [NotNull]
        internal List<Player> this[int x] => _board[x];

        [NotNull]
        [ItemNotNull]
        internal IReadOnlyList<IReadOnlyList<Player>> BoardField => _board;

        [NotNull]
        internal IReadOnlyList<Point> EmptyCells
        {
            get
            {
                var emptyCells = new List<Point>();
                for (var indexOfRow = 0; indexOfRow < _board.Count; indexOfRow++)
                {
                    var line = _board[indexOfRow];
                    for (var indexOfColumn = 0; indexOfColumn < line.Count; indexOfColumn++)
                    {
                        var cell = line[indexOfColumn];
                        if (cell == Player.Empty)
                        {
                            emptyCells.Add(new Point(indexOfColumn, indexOfRow));
                        }
                    }
                }

                return emptyCells;
            }
        }

        private bool IsPlayerWin(Player player)
        {
            var isWinByDiagonal = true;
            var isWinByBackDiagonal = true;
            var linesCount = _board.Count - 1;
            for (var i = 0; i <= linesCount; i++)
            {
                var isWinByVertical = true;
                var isWinByHorizontal = true;
                var line = _board[i];
                for (var j = 0; j < line.Count; j++)
                {
                    if (line[j] != player)
                    {
                        isWinByVertical = false;
                    }

                    if (_board[j][i] != player)
                    {
                        isWinByHorizontal = false;
                    }
                }

                if (isWinByHorizontal ||
                    isWinByVertical)
                {
                    return true;
                }

                if (line[i] != player)
                {
                    isWinByDiagonal = false;
                }

                if (line[linesCount - i] != player)
                {
                    isWinByBackDiagonal = false;
                }
            }

            return isWinByDiagonal || isWinByBackDiagonal;
        }

        internal Solution GetWinner()
        {
            if (IsPlayerWin(Player.X))
            {
                return Solution.XWin;
            }

            if (IsPlayerWin(Player.O))
            {
                return Solution.OWin;
            }

            return _board.SelectMany(line => line).All(p => p != Player.Empty) ? Solution.Draw : Solution.Unknown;
        }

        [NotNull]
        internal Board GetCopy()
        {
            var newBoard = new List<List<Player>>();
            foreach (var line in _board)
            {
                var players = new List<Player>(line);
                newBoard.Add(players);
            }

            return new Board(newBoard);
        }

        [NotNull]
        [MustUseReturnValue]
        internal Board PlayerOn(Point coordinates, Player player)
        {
            var board = GetCopy();
            board[coordinates.Y][coordinates.X] = player;
            return board;
        }

        [NotNull]
        [ItemNotNull]
        private static List<List<Player>> InitializeBoard(int capacity)
        {
            var board = new List<List<Player>>();
            for (var i = 0; i < capacity; i++)
            {
                var line = new List<Player>();
                for (var j = 0; j < capacity; j++)
                {
                    line.Add(Player.Empty);
                }

                board.Add(line);
            }

            return board;
        }
    }
}