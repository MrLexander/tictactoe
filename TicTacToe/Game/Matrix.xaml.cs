﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Guard;
using JetBrains.Annotations;

namespace TicTacToe.Game
{
    public sealed partial class Matrix

    {
        [NotNull] private readonly Dictionary<Button, Point> _controls = new Dictionary<Button, Point>();

        internal Matrix()
        {
            InitializeComponent();
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            MatrixChanged();
            base.OnRender(drawingContext);
        }

        private void MatrixChanged()
        {
            var mainGrid = MainGrid;
            Debug.Assert(mainGrid != null, nameof(mainGrid) + " != null");

            ClearMainGrid();

            Debug.Assert(Value != null, nameof(Value) + " != null");
            var martrix = Value.BoardField;

            for (var rowIndex = 0; rowIndex < martrix.Count; rowIndex++)
            {
                mainGrid.RowDefinitions.Add(new RowDefinition());
                var row = martrix[rowIndex];

                var internalGrid = GenerateRowGrid(rowIndex, row);
                mainGrid.Children.Add(internalGrid);
            }
        }

        private void ClearMainGrid()
        {
            Debug.Assert(MainGrid != null, nameof(MainGrid) + " != null");

            MainGrid.RowDefinitions.Clear();
            MainGrid.ColumnDefinitions.Clear();
            MainGrid.Children.Clear();
        }

        [NotNull]
        private Grid GenerateRowGrid(int rowIndex, [NotNull] IReadOnlyList<Player> row)
        {
            var internalGrid = new Grid();
            Grid.SetRow(internalGrid, rowIndex);
            for (var columnIndex = 0; columnIndex < row.Count; columnIndex++)
            {
                internalGrid.ColumnDefinitions.Add(new ColumnDefinition {Width = new GridLength(1, GridUnitType.Star)});

                var cell = row[columnIndex];
                var control = CreateControl(cell);
                internalGrid.Children.Add(control);
                Grid.SetColumn(control, columnIndex);

                var button = (Button) control.Child;
                Debug.Assert(button != null, nameof(button) + " != null");

                _controls.Add(button, new Point(columnIndex, rowIndex));
            }

            return internalGrid;
        }

        [NotNull]
        private Viewbox CreateControl(Player cell)
        {
            var button = new Button {Content = cell.GetCellText()};
            button.Click += ButtonOnClick;
            if (cell != Player.Empty)
            {
                button.IsEnabled = false;
            }

            var viewbox = new Viewbox
            {
                Stretch = Stretch.Fill,
                VerticalAlignment = VerticalAlignment.Stretch,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Child = button
            };

            return viewbox;
        }

        private void ButtonOnClick([NotNull] object sender, RoutedEventArgs e)
        {
            ThrowIf.Argument.IsNull(sender, nameof(sender));

            var clickedButton = (Button) sender;
            Debug.Assert(clickedButton != null, nameof(clickedButton) + " != null");

            var point = _controls[clickedButton];

            if (Command?.CanExecute(point) != true)
            {
                return;
            }

            Command.Execute(point);
        }

        #region Command

        [NotNull] internal static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register(nameof(Command), typeof(ICommand), typeof(Matrix));

        [CanBeNull]
        internal ICommand Command
        {
            get => (ICommand) GetValue(CommandProperty);
            set => SetValue(CommandProperty, value);
        }

        #endregion

        #region Value

        [NotNull] internal static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register(nameof(Value),
                                        typeof(Board),
                                        typeof(Matrix),
                                        new UIPropertyMetadata(new Board(3), ValueChangedCallback));

        private static void ValueChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var matrix = (Matrix) d;
            Debug.Assert(matrix != null, nameof(matrix) + " != null");
            matrix.InvalidateVisual();
        }

        [CanBeNull]
        internal Board Value
        {
            get => (Board) GetValue(ValueProperty);
            set => SetValue(ValueProperty, value);
        }

        #endregion
    }
}