﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Guard;
using JetBrains.Annotations;

namespace TicTacToe.Game
{
    internal sealed class ViewModel : INotifyPropertyChanged, IComputerStepProvider
    {
        [NotNull] private readonly IModel _model;

        internal ViewModel([NotNull] IModel model)
        {
            ThrowIf.Argument.IsNull(model, nameof(model));
            _model = model;

            PlayerStepCommand = new PlayerStepCommand(model, this);
        }

        [NotNull]
        public Board Board => _model.Board;

        public ICommand PlayerStepCommand { get; }

        public void Step()
        {
            _model.ComputerStep();
            OnPropertyChanged(nameof(Board));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        internal event EventHandler<PlayerWinArgs> PlayerWin
        {
            add => _model.PlayerWin += value;
            remove => _model.PlayerWin -= value;
        }

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}