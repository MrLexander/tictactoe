﻿using System;

namespace TicTacToe.Game
{
    internal interface IModel : IBoardProvider
    {
        void ComputerStep();
        event EventHandler<PlayerWinArgs> PlayerWin;
    }
}