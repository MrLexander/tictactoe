﻿using System;

namespace TicTacToe.Game
{
    internal sealed class Model : IModel
    {
        internal Model(int capacity, int level)
        {
            Level = level;
            Board = new Board(capacity);
        }

        private int Level { get; }

        public Board Board { get; private set; }

        public event EventHandler<PlayerWinArgs> PlayerWin;

        public void PlayerOn(Point coordinates, Player player)
        {
            Board = Board.PlayerOn(coordinates, player);
        }

        public void ComputerStep()
        {
            var winner = Board.GetWinner();
            if (winner != Solution.Unknown)
            {
                PlayerWin?.Invoke(this, new PlayerWinArgs(winner));
                return;
            }

            var optimalWayAlgorithm = new OptimalWayAlgorithm(Level);
            var computerStep = optimalWayAlgorithm.Get(Board);

            PlayerOn(computerStep, Player.O);
            winner = Board.GetWinner();
            if (winner != Solution.Unknown)
            {
                PlayerWin?.Invoke(this, new PlayerWinArgs(winner));
            }
        }
    }
}