﻿using System;
using System.Windows;
using Guard;
using JetBrains.Annotations;
using TicTacToe.Resources;

namespace TicTacToe.Game
{
    public sealed partial class View
    {
        internal View([NotNull] ViewModel viewModel)
        {
            ThrowIf.Argument.IsNull(viewModel, nameof(viewModel));
            DataContext = viewModel;
            viewModel.PlayerWin += OnPlayerWin;

            InitializeComponent();
        }

        private void OnPlayerWin(object sender, [NotNull] PlayerWinArgs args)
        {
            ThrowIf.Argument.IsNull(args, nameof(args));

            string message;
            switch (args.Winner)
            {
                case Solution.XWin:
                    message = Strings.XWin;
                    break;
                case Solution.Draw:
                    message = Strings.Draw;
                    break;
                case Solution.OWin:
                    message = Strings.OWin;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            MessageBox.Show(message, Strings.EndOfGame);
            Close();
        }
    }
}