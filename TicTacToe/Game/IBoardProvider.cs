﻿using JetBrains.Annotations;

namespace TicTacToe.Game
{
    internal interface IBoardProvider
    {
        [NotNull]
        Board Board { get; }

        void PlayerOn(Point coordinates, Player payer);
    }
}