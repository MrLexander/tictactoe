﻿namespace TicTacToe.Game
{
    internal sealed class PlayerWinArgs
    {
        public PlayerWinArgs(Solution winner)
        {
            Winner = winner;
        }

        internal Solution Winner { get; }
    }
}