﻿namespace TicTacToe.Game
{
    internal interface IComputerStepProvider
    {
        void Step();
    }
}