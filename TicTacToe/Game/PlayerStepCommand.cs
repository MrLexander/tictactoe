﻿using System;
using System.Windows.Input;
using Guard;
using JetBrains.Annotations;

namespace TicTacToe.Game
{
    internal sealed class PlayerStepCommand : ICommand
    {
        [NotNull] private readonly IBoardProvider _boardProvider;
        [NotNull] private readonly IComputerStepProvider _computerStepProvider;

        internal PlayerStepCommand([NotNull] IBoardProvider boardProvider, [NotNull] IComputerStepProvider computerStepProvider)
        {
            ThrowIf.Argument.IsNull(computerStepProvider, nameof(computerStepProvider));
            ThrowIf.Argument.IsNull(boardProvider, nameof(boardProvider));

            _boardProvider = boardProvider;
            _computerStepProvider = computerStepProvider;
        }

        public bool CanExecute([NotNull] object parameter)
        {
            ThrowIf.Argument.IsNull(parameter, nameof(parameter));
            var coordinates = (Point) parameter;

            return _boardProvider.Board[coordinates.Y][coordinates.X] == Player.Empty;
        }

        public void Execute([NotNull] object parameter)
        {
            ThrowIf.Argument.IsNull(parameter, nameof(parameter));
            var coordinates = (Point) parameter;

            _boardProvider.PlayerOn(coordinates, Player.X);
            _computerStepProvider.Step();
        }

        public event EventHandler CanExecuteChanged;

        [UsedImplicitly]
        private void OnCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}