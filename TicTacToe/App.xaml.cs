﻿using System.Windows;

namespace TicTacToe
{
    public sealed partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            var starter = new Starter();
            starter.Invoke();
        }
    }
}