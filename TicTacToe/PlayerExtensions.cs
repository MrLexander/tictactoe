﻿using System.Linq;
using System.Reflection;
using Guard;

namespace TicTacToe
{
    internal static class PlayerExtensions
    {
        internal static string GetCellText(this Player cell)
        {
            var type = typeof(Player);
            var memberInfo = type.GetMember(cell.ToString()).FirstOrDefault();
            ThrowIf.Variable.IsNull(memberInfo, nameof(memberInfo));

            var cellTextAttribute = memberInfo.GetCustomAttribute<CellAttribute>();
            ThrowIf.Variable.IsNull(cellTextAttribute, nameof(cellTextAttribute));

            return cellTextAttribute.Text;
        }
    }
}